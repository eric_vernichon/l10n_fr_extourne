# -*- encoding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2007-2013  Eric Vernichon <eric@vernichon.fr> All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################


from osv.orm import TransientModel, except_orm
from osv import fields
from tools.translate import _
import logging

 

class move_extourne(TransientModel):
    _name = 'move.extourne'
    _description = 'Move Extourne'
    _columns = {
               
                'move_ids':fields.many2many('account.move','','','','Account Move'),
    }
    def extourne(self, cr, uid, ids, context=None):
        
        move_ids =  self.read(cr,uid,ids)[0]['move_ids']
        new_move_ids = list(move_ids)
        moves = self.pool.get('account.move').read(cr,uid,move_ids)
        for move_id  in move_ids :
            move = self.pool.get('account.move').read(cr,uid,move_id)
            print "vmove ", move
            mvtextourne_id  = self.pool.get('account.move').copy(cr,uid,move_id)
            mvtextourne = {} 
 
            mvtextourne['name']  = u"contrepassation de l'écriture n°"+move['name']
            mvtextourne['state'] = 'draft'
            mvext = self.pool.get('account.move').write(cr,uid,[mvtextourne_id],mvtextourne)
            new_move_ids.append(mvtextourne_id)
            line_ids = self.pool.get('account.move.line').search(cr,uid,[('move_id','=',mvtextourne_id)])
            for line_id in line_ids:
                line = self.pool.get('account.move.line').read(cr,uid,line_id)
                self.pool.get('account.move.line').write(cr,uid,line_id,{'debit' : line['credit'], 'credit' : line['debit'], 'reconcile_id': None})
 
        return {
                'name': _("extourne move"),
                'view_type': 'form',           
                'view_mode': 'tree,form',      
                'res_model': 'account.move',
                'type': 'ir.actions.act_window',
                'domain': "[('id', 'in', %s)]" % new_move_ids,
                           
            }

move_extourne()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

